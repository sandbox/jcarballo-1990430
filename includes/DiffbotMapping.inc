<?php
/**
 * @file DiffbotMapping class.
 *
 */

class DiffbotMapping {
  public $id;

  public $contentType;

  public $fields = array(
    'default_fields' => array(
      'url' => array(
        'title' => 'URL Target',
        'description' => 'Field where get the URL to analyze. FIELD TYPE: Text',
        'type' => 'text',
      ),
      'resolved_url' => array(
        'title' => 'Resolved URL',
        'description' => 'Field where save the resolving URL is different from
                        the destination URL',
        'type' => 'text',
      ),
      'title' => array(
        'title' => 'Title',
        'description' => 'Save the extracted title in the node title field.',
        'type' => 'title',
      ),
      'summary' => array(
        'title' => 'Summary',
        'description' => 'Field where is saved the summary extracted.',
        'type' => 'text',
      ),
      'text' => array(
        'title' => 'Body - plain text',
        'description' => 'Field where the plain text will be saved.',
        'type' => 'text',
      ),
      'html' => array(
        'title' => 'Body - HTML',
        'description' => 'Field where is saved the html extracted.',
        'html_filter' => '',
        'type' => 'text',
      ),
      'tags' => array(
        'title' => 'Tags',
        'description' => 'Field where is saved the tags generated.',
        'type' => 'terms',
      ),
      'author' => array(
        'title' => 'Author',
        'description' => 'Field where is saved the author extracted.',
        'type' => 'terms',
      ),
      'source' => array(
        'title' => 'Source',
        'description' => 'The site name where the data is extracted. Improve the name result enabling the Meta Field in Diffbot API Setting</a>. The field type have to be a <b>Taxonomy field</b>.
          The Vocabulary chosen should have a field to save the url. <b>Field
          machine name: field_url_homepage</b>',
        'type' => 'terms'
      ),
    ),
  );

  public static function withForm($form_state) {
    $mapping = new DiffbotMapping();
    $mapping->fillForm($form_state);
    return $mapping;
  }

  public static function withID($id) {
    $mapping = new DiffbotMapping();

    if (!empty($id)) {
      $mapping->id = $id;
      if ($mapping->exist()) {
        $mapping_fields = $mapping->select();
        $mapping->fillDB($mapping_fields);
      }
    }
    return $mapping;
  }

  public static function withNodeType($node_type) {
    $mapping = new DiffbotMapping();

    $mapping_fields = array();

    if (!empty($node_type)) {
      $mapping->contentType = $node_type;
      if ($mapping->exist()) {
        $mapping_fields = $mapping->select();
        $mapping->fillDB($mapping_fields);
      }
    }
    return $mapping;
  }

  protected function fillForm($form_state) {

    if (isset($form_state['values']['id'])) {
      $this->id = $form_state['values']['id'];
    }

    $this->contentType = $form_state['values']['content_type'];

    // Adding the default fields.
    foreach ($this->fields['default_fields'] as $field => $value) {
      $this->setValue($field, $form_state['values'][$field]);
      if ($field == 'html') {
        $this->fields['default_fields']['html']['html_filter']
          = $form_state['values']['html_filter'];
      }
    }
  }

  protected function fillDB($mapping_fields) {
    $this->contentType = $mapping_fields['content_type'];

    foreach ($this->fields['default_fields'] as $field => $value) {
      $this->setValue($field, $mapping_fields[$field]);
      if ($field == 'html') {
        $this->fields['default_fields']['html']['html_filter']
          = $mapping_fields['html_filter'];
      }
    }
  }

  public function exist() {
    $exist = FALSE;
    if (!is_null($this->id)) {
      $row = $this->select();
    }
    elseif (!is_null($this->contentType)) {
      $field = array(
        'name' => 'content_type',
        'value' => $this->contentType,
      );
      $row = $this->selectBy($field);
    }
    $exist = !empty($row) ? TRUE : FALSE;

    return $exist;
  }

  public function save() {
    if ($this->exist()) {
      $this->edit();
    }
    else {
      $this->add();
    }
  }

  protected function add() {
    $values['content_type'] = $this->contentType;
    $values = array_merge($values, $this->prepareValuesToSave());
    $this->withID($this->insert($values));
  }

  protected function edit() {
    $values = $this->prepareValuesToSave();
    $this->update($values, $this->id);
  }

  protected function insert($values) {
    return db_insert('diffbot_mapping')
      ->fields($values)
      ->execute();
  }

  protected function update($values, $id) {
    return db_update('diffbot_mapping')
      ->fields($values)
      ->condition('id', $id, '=')
      ->execute();
  }

  public function delete() {
    db_delete('diffbot_mapping')
      ->condition('id', $this->id, '=')
      ->execute();
  }

  protected function select() {
    $db_result = db_select('diffbot_mapping', 'm')
      ->fields('m')
      ->condition('id', $this->id, '=')
      ->execute()
      ->fetchAssoc();
    return $db_result;
  }

  protected function selectBy($field) {
    $db_result = db_select('diffbot_mapping', 'm')
      ->fields('m')
      ->condition($field['name'], $field['value'])
      ->execute()
      ->fetchAssoc();
    $this->id = empty($db_result) ? NULL : $db_result['id'];
    return $db_result;
  }

  protected function setValue($element, $value) {
    if (array_key_exists($element, $this->fields['default_fields'])) {
      $this->fields['default_fields'][$element]['assigned_field'] = $value;
    }
    return $this;
  }

  public function get_value($element) {
    if (array_key_exists($element, $this->fields['default_fields'])) {
      $value = $this->fields['default_fields'][$element]['assigned_field'];
    }
    return $value;
  }

  public function get_description($element) {
    $description = array_key_exists($element, $this->fields['default_fields']) ?
      $this->fields['default_fields'][$element]['description'] : '';
    return $description;
  }

  public function getFieldTitle($element) {
    $title = array_key_exists($element, $this->fields['default_fields']) ?
      $this->fields['default_fields'][$element]['title'] : '';
    return $title;
  }

  protected function prepareValuesToSave() {
    $values = array();
    foreach ($this->fields['default_fields'] as $field => $value) {
      $values[$field] = $this->get_value($field);
      if ($field == 'html') {
        $values['html_filter'] = $value['html_filter'];
      }
    }
    return $values;
  }
}

function diffbot_objectToArray($d) {
  if (is_object($d)) {
    $d = get_object_vars($d);
  }
  if (is_array($d)) {
    return array_map(__FUNCTION__, $d);
  }
  else {
    return $d;
  }
}
