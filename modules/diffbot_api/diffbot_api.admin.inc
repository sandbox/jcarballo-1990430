<?php
/**
 * @file diffbot_api.admin.inc
 *
 */

/**
 * Build the admin settings form.
 */
function diffbot_api_admin_settings() {
  $diffbot_url = array(
    '!diffboturl' => l(t('Need One'), 'http://www.diffbot.com/pricing/',
      array(
        'attributes' => array(
          'target' => 'blank',
        ),
      )
    ),
  );

  $diffbot_api_article_doc = array(
    '!diffbotArticleDoc' => l(t('Article API Documentation'), 'http://www.diffbot.com/dev/docs/article',
      array(
        'attributes' => array(
          'target' => 'blank',
        ),
      )
    ),
  );

  $form['diffbot_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Diffbot Developer Token'),
    '#default_value' => variable_get('diffbot_api_token', NULL),
    '#size' => 40,
    '#description' => t('Required to utilize the Diffbot service. !diffboturl', $diffbot_url),
  );

  $form['diffbot_api'] = array(
    '#type' => 'fieldset',
    '#title' => 'API Article. Optional arguments',
    '#collapsible' => FALSE,
  );

  $form['diffbot_api']['diffbot_api_article_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Fields',
    '#description' => t('Used to control which fields are returned by the API. See the Response section below. More details in !diffbotArticleDoc', $diffbot_api_article_doc),
    '#default_value' => variable_get('diffbot_api_article_fields', array()),
    '#options' => array(
      'meta' => t('Meta'),
      'querystring' => t('Query string'),
      'links' => t('Links'),
      'humanLanguage' => t('Human Language'),
    ),
  );

  return system_settings_form($form);
}
