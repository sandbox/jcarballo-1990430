<?php

/**
 * @file Helpers function to diffbot module.
 *
 */

function diffbot_load_mapping($node_type) {
  return DiffbotMapping::withNodeType($node_type);
}

/**
 * Check if a Diffbot response is valid.
 * @see http://www.diffbot.com/dev/docs/error/
 * @param  array $diffbot_response
 *           The Diffbot response.
 * @return array
 *           Contains the info about the validation
 */
function diffbot_response_is_valid($diffbot_response) {
  $validation = array(
    'type' => 'success',
    'is_valid' => TRUE,
    'message' => 'Text has been extracted correctly.'
  );

  if (empty($diffbot_response)) {
    $validation = array(
      'type' => 'error',
      'is_valid' => FALSE,
      'message' => 'No response from Diffbot Service.'
    );
  }
  elseif (isset($diffbot_response['error_code'])) {
    $error_code = $diffbot_response['error_code'];
    switch ($error_code) {
      case 401:
        $validation = array(
          'type' => 'error',
          'is_valid' => FALSE,
          'message' => 'Unauthorized Diffbot API Token.',
        );
        break;

      case 404:
        $validation = array(
          'type' => 'error',
          'is_valid' => FALSE,
          'message' => 'Requested page not found',
        );
        break;

      case 429:
        $validation = array(
          'type' => 'error',
          'is_valid' => FALSE,
          'message' => 'Your token has exceeded the allowed number of calls, or has otherwise been throttled for API abuse.',
        );
        break;

      case 500:
        $validation = array(
          'type' => 'error',
          'is_valid' => FALSE,
          'message' => 'Error processing the page. Specific information will be returned in the JSON response.',
        );
        break;
    }
  }
  return $validation;
}

function diffbot_node_field_fill($node, $field_attrs, $extracted_data) {
  if (!empty($extracted_data)) {
    switch ($field_attrs['type']) {
      case 'title':
        $node->title = $extracted_data;
        break;

      case 'text':
        diffbot_node_text_field_add($node, $field_attrs, $extracted_data);
        break;

      case 'terms':
        diffbot_node_terms_field_add($node, $field_attrs, $extracted_data);
        break;
    }
  }
  return $node;
}


function diffbot_node_terms_field_add($node, $field_attrs, $extracted_data) {
  $field_info = field_info_field($field_attrs['assigned_field']);
  $vocab_machine_name
    = $field_info['settings']['allowed_values'][0]['vocabulary'];
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocab_machine_name);

  unset($node->{$field_attrs['assigned_field']});
  $terms = array();

  if ($field_attrs['title'] == 'Source') {
    $extracted_data['vocabulary'] = $vocabulary;
    $extracted_data['language'] = $node->language;
    $terms[] = $extracted_data;
  }
  else {
    foreach ($extracted_data as $value) {
      $term['name'] = $value;
      $term['vocabulary'] = $vocabulary;
      $terms[] = $term;
    }
  }
  foreach ($terms as $term) {
    diffbot_save_taxonomy_term($node, $field_attrs['assigned_field'], $term);
  }

}

function diffbot_save_taxonomy_term($node, $field, $term) {
  if ($terms = taxonomy_get_term_by_name($term['name'], $term['vocabulary']->machine_name)) {
    $terms_keys = array_keys($terms);
    $node->{$field}[$node->language][]['tid'] = $terms_keys[0];
  }
  else {
    $tid = diffbot_create_taxonomy_term($term);
    $node->{$field}[$node->language][]['tid'] = $tid;
  }
}

/**
 * Create a taxonomy term and return the tid.
 */
function diffbot_create_taxonomy_term($term_data) {
  $term = new stdClass();
  $term->name = $term_data['name'];
  $term->vid = $term_data['vocabulary']->vid;
  if (isset($term_data['term_fields'])) {
    foreach ($term_data['term_fields'] as $field_name => $field_value) {
      $term->{$field_name}[$term_data['language']][0]['value'] = $field_value;
    }
  }
  taxonomy_term_save($term);
  return $term->tid;
}

function diffbot_node_text_field_add($node, $field_attrs, $extracted_data) {
  $node->{$field_attrs['assigned_field']}[$node->language][0]['value']
    = $extracted_data;
  if ($field_attrs['assigned_field'] == 'body') {
    $node->body[$node->language][0]['format'] = isset($field_attrs['html_filter'])
      ? $field_attrs['html_filter'] : NULL;
  }
}

function diffbot_mapping_schema_build($fields, $field_type) {
  $schema_fields = array();
  foreach ($fields as $field => $attrs) {
    $schema_fields[$field] = array(
      'description' => $attrs['description'],
      'type' => $field_type,
      'length' => '255',
    );
  }
  return $schema_fields;
}

function diffbot_schema_add_fields($schema, $fields) {
  $schema['diffbot_mapping']['fields']
    = array_merge($schema['diffbot_mapping']['fields'], $fields);
  return $schema;
}

function diffbot_mappings_get() {
  $mappings = array();
  $db_result = db_select('diffbot_mapping', 'm')
      ->fields('m', array('id'))
      ->execute()
      ->fetchAssoc();

  if (!empty($db_result)) {
    foreach ($db_result as $id) {
      $mappings[] = DiffbotMapping::withID($id);
    }
  }
  return $mappings;
}

function _diffbot_config_set_data($array_mapping) {
  $mapping = new DiffbotMapping();
  $mapping->contentType = $array_mapping['contentType'];
  $mapping->fields = $array_mapping['fields'];
  $mapping->save();
}

/**
 * Reports the process status to user interface, watchlog and diffbot log.
 * @param  $node       [description]
 * @param  [type] $mapping    [description]
 * @param  [type] $validation [description]
 * @return [type]             [description]
 */
function diffbot_report_process($node, $mapping, $validation) {
  $id = !empty($node->nid) ? $node->nid : uniqid();
  $drupal_message_type = ($validation['type'] == 'success') ? 'status' : 'error';
  $watchdog_type = ($validation['type'] == 'success') ? WATCHDOG_INFO : WATCHDOG_ERROR;
  $watchdog_message = 'Node ID: ' .  $id . '. ' . $validation['message'];

  drupal_set_message(t(check_plain($validation['message'])), $drupal_message_type);

  watchdog('diffbot',
           t(check_plain($watchdog_message)),
           array(),
           $watchdog_type, NULL);

  diffbot_log($mapping->id, $node->nid, $validation['message'], $validation['type']);
}


/**
 * Save the log event in the database
 *
 * @param  int $mapping_id
 *           The Diffbot Mapping ID used to save the data.
 * @param  int $node_id
 *           Node ID where the extracted data is saved.
 * @param  string $message
 *           Description about the event.
 * @param  string $type
 *           Type of event log.
 */
function diffbot_log($mapping_id, $node_id, $message, $type) {
  db_insert('diffbot_log')
    ->fields(array(
      'mid' => $mapping_id,
      'diffbot_nid' => $node_id,
      'log_time' => time(),
      'message' => $message,
      'message_type' => $type,
    ))
    ->execute();
}
