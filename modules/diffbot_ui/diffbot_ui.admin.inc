<?php
/**
 * @file
 * diffbot.admin.inc
 */

function diffbot_ui_overview() {

  $header_table_edit = array(
    array('data' => t('Attached to')),
    array('data' => t('Operation')),
  );
  $query = db_select('diffbot_mapping', 'n')
    ->fields('n', array('id', 'content_type'))
    ->execute()
    ->fetchAll();

  $rows_table_edit[] = array();

  if (count($query) != 0) {
    foreach ($query as $record_edit_table) {
      $rows_table_edit[] = array(
        array(
          'data' => l($record_edit_table->content_type, 'admin/config/services/diffbot/mapping/' . $record_edit_table->id),
        ),
        array(
          'data' =>
          l(t('edit'), 'admin/config/services/diffbot/mapping/' .
            $record_edit_table->id . '/edit') . " - " .
          l(t('delete'), 'admin/config/services/diffbot/mapping/' .
            $record_edit_table->id . '/delete'),
        ),
      );
    }
  }

  $caption_table_edit = t('Table for edit Diffbot mapping');
  return theme('table', array(
          'header' => $header_table_edit,
          'rows' => $rows_table_edit,
          'caption' => $caption_table_edit,
          ));
}
