<?php
/**
 * @file
 * diffbot_ui.forms.inc
 */
module_load_include('inc', 'diffbot_ui', 'includes/diffbot_ui.forms.helpers');

/**
 * [diffbot_ui_mapping_save_form description]
 * @param  [type] $form       [description]
 * @param  [type] $form_state [description]
 * @return [type]             [description]
 */
function diffbot_ui_mapping_save_form($form, &$form_state) {
  $form = diffbot_ui_form_builder($form, $form_state, '');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Implements hook_validate()
 **/
function diffbot_ui_mapping_save_form_validate($form, &$form_state) {
  diffbot_ui_mapping_form_valid_values($form_state);
}

/**
 * Implements hook_submit()
 */
function diffbot_ui_mapping_save_form_submit($form, &$form_state) {
  $mapping = DiffbotMapping::withForm($form_state);
  $mapping->save();

  //TODO: add custom validation to content_type_node_form
  //diffbot_ui_content_type_node_form_requirements_add($mapping->contentType);

  watchdog('diffbot', 'attached mapping to %title correctly.',
    array('%title' => $mapping->contentType));

  drupal_set_message( t("<strong>The mapping has been attached to %title correctly.</strong>", array('%title' => $mapping->contentType)));

  drupal_goto("admin/config/services/diffbot/mapping");
}


/**
 * [diffbot_ui_mapping_edit_form description]
 * @param  [type] $form       [description]
 * @param  [type] $form_state [description]
 * @param  [type] $id_mapping [description]
 * @return [type]             [description]
 */
function diffbot_ui_mapping_edit_form($form, &$form_state, $id_mapping) {
  $form  = diffbot_ui_form_builder($form, $form_state, $id_mapping);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Implements hook_validate()
 */
function diffbot_ui_mapping_edit_form_validate($form, &$form_state) {
  diffbot_ui_mapping_form_valid_values($form_state);
}

/**
* Implements hook_submit().
**/
function diffbot_ui_mapping_edit_form_submit($form, &$form_state) {
  $mapping = DiffbotMapping::withForm($form_state);
  $mapping->save();

  watchdog('diffbot', 'updated mapping attached to %title correctly.',
    array('%title' => $mapping->contentType));

  drupal_set_message(
    t("<strong>The mapping attached to %title has been updated</strong>",
      array('%title' => $mapping->contentType)));

  drupal_goto("admin/config/services/diffbot/mapping");
}

/**
 * [diffbot_ui_mapping_delete_form description]
 * @param  [type] $form       [description]
 * @param  [type] $form_state [description]
 * @param  [type] $id_mapping [description]
 * @return [type]             [description]
 */
function diffbot_ui_mapping_delete_form($form, &$form_state, $id_mapping) {
  $mapping = DiffbotMapping::withID($id_mapping);

  $form['mapping'] = array(
  '#type' => 'value',
  '#value' => $mapping,
  );

  $form['mapping_name'] = array('#type' => 'value', '#value'
    => $mapping->contentType);

  $form['mid'] = array('#type' => 'value', '#value' => $id_mapping);

  return confirm_form($form,
    t('Are you sure you want to delete the mapping attached to %title?',
      array('%title' => $mapping->contentType)),
    'admin/config/services/diffbot/mapping/',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
* Implements hook_submit().
**/
function diffbot_ui_mapping_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $mapping = $form_state['values']['mapping'];
    $mapping->delete();

  //TODO: remove custom validation to content_type_node_form
  //diffbot_ui_content_type_node_form_requirements_delete($mapping->contentType);

    watchdog('diffbot', 'deleted mapping attached to %title.',
      array('%title' => $form_state['values']['mapping_name']));

    drupal_set_message(t('Mapping attached to %title has been deleted.',
      array('%title' => $form_state['values']['mapping_name'])));

    $form_state['redirect'] = 'admin/config/services/diffbot/mapping';
  }
}
