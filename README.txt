DIFFBOT

INTRO
=====

This module integrates Drupal with the Diffbot API Web-Service. The web
service allows extract clean article text from news article web pages.


INSTALLATION
============

1) Place this module directory into your Drupal modules directory.

2) Enable the Diffbot, and Diffbot API module in Drupal, at:
   administration -> site configuration -> modules (admin/build/modules)

3) Obtain Diffbot API Token from their website:
   http://www.diffbot.com/login/

4) Add Diffbot Token and tune other settings at:
   administration -> site configuration -> Web servicies -> Diffbot
   (admin/settings/diffbot/api)

5) Set Up fields on of your content types to be used at:
administration -> site configuration -> Web services -> Diffbot -> Diffbot
Mapping admin/config/services/diffbot/mapping/create

CREDITS
========
Written by
  - Jorge Carballo <info at jorgecarballo dot com>
