<?php
/**
 * @file Class DiffbotApiService
 *
 */

class DiffbotApiService {

  const API_URL = 'http://api.diffbot.com/';

  const ARTICLE_API = 'v2/article';

  protected $token;

  protected $api_fields;


  public function __construct() {
    $this->setToken(variable_get('diffbot_api_token'));
    $api_fields = variable_get('diffbot_api_api_fields', array());
    $this->setApiFields($api_fields);
  }

  public function getArticle($url) {
    $article = $this->getPage($url, self::ARTICLE_API);
    $article = $this->getResolvedUrl($article);
    $article['source'] = $this->getSourceName($article);
    return $article;
  }

  protected function getPage($url, $api) {
    $targetUrl = $this->prepareUrl($url, $api);
    $response = drupal_http_request($targetUrl);
    return drupal_json_decode($response->data, TRUE);
  }

  protected function prepareUrl($url, $type) {
    $arguments = array(
      'token' => $this->getToken(),
      'url'   => $url
    );

    $finalUrl  = self::API_URL . $type;
    $finalUrl .= '?' . http_build_query($arguments);
    $finalUrl .= $this->getApiFields();

    return $finalUrl;
  }

  protected function setToken($token) {
    $this->token = $token;
    return $this;
  }

  public function getToken() {
    return $this->token;
  }

  public function setApiFields($api_fields) {
    $this->api_fields = $api_fields;
  }

  public function getApiFields() {
    $api_fields = array();

    foreach ($this->api_fields as $field) {
      if (!empty($field)) {
        $api_fields[] = $field;
      }
    }

    $argument = (!empty($api_fields)) ? '&fields=' . implode(',', $api_fields) : NULL ;

    return $argument;
  }

  protected function getResolvedUrl($article) {
    if (!isset($article['resolved_url'])) {
      $article['resolved_url'] = $article['url'];
    }
    return $article;
  }

  protected function getSourceName($article) {
    $url = parse_url($article['resolved_url']);
    $source_url = $url['host'];

    $source_name = isset($article['meta']['og']['og:site_name'])
    ? $article['meta']['og']['og:site_name']
    : str_replace('www.', '', $source_url);

    $source = array(
      'name' => $source_name,
      'term_fields' => array(
        'field_url_homepage'  => $url['scheme'] . '://' . $source_url,
      ),
    );

    return $source;
  }


}
