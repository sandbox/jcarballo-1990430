<?php
/**
 * @file
 * forms.helpers.inc
 */

function diffbot_ui_form_builder($form, &$form_state, $id_mapping) {
  $options_first = diffbot_ui_get_content_types();
  $mapping = DiffbotMapping::withID($id_mapping);

  if (($mapping->id)) {
    $mapping_content_type = $mapping->contentType;
    $content_types = node_type_get_types();
    $options_first = array(
      $mapping_content_type => $content_types[$mapping_content_type]->name,
    );
  }

  $selected = isset($form_state['values']['content_type']) ?
    $form_state['values']['content_type'] : key($options_first);

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $mapping->id,
  );
  $form['content_type'] = array(
    '#title' => 'Content Type',
    '#description' => t('Select the content type to attach with Diffbot'),
    '#options' => $options_first,
    '#type' => 'select',
    '#ajax' => array(
      'callback' => 'diffbot_ui_get_field_default_fieldset',
      'wrapper' => 'diffbot-fields',
    ),
  );

  $form['diffbot_mapping']['default_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Mapping Fields'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('id' => 'diffbot-fields'),
  );
  $form = diffbot_ui_form_set_title($form, $mapping);
  $form = diffbot_ui_form_set_description($form, $mapping);
  $form = diffbot_ui_form_set_type($form, $mapping);
  $form = diffbot_ui_form_set_weight($form, $mapping);
  $form = diffbot_ui_form_set_options_values($form, $selected);
  $form = diffbot_ui_form_set_particular_attributes($form);
  $form = diffbot_ui_form_set_custom_elements($form);
  if (!is_null($mapping->id)) {
    $form = diffbot_ui_form_set_default_values($form, $mapping);
  }
  if (!empty($mapping->fields['custom_fields'])) {
    $form = diffbot_ui_custom_fields_form_build($form, $mapping, $selected);
  }
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/services/diffbot/mapping'),
    '#weight' => 20,
  );
  return $form;
}

function diffbot_ui_form_set_title($form, $mapping) {
  foreach ($mapping->fields['default_fields'] as $field => $value) {
    $form['diffbot_mapping']['default_fields'][$field]['#title'] =
      $mapping->getFieldTitle($field);
  }
  return $form;
}

function diffbot_ui_form_set_description($form, $mapping) {
  foreach ($mapping->fields['default_fields'] as $field => $value) {
    $form['diffbot_mapping']['default_fields'][$field]['#description'] =
      $mapping->get_description($field);
  }
  return $form;
}

function diffbot_ui_form_set_type($form, $mapping) {
  foreach ($mapping->fields['default_fields'] as $field => $value) {
    $form['diffbot_mapping']['default_fields'][$field]['#type'] = 'select';
  }
  return $form;
}


function diffbot_ui_form_set_options_values($form, $selected) {
  foreach ($form['diffbot_mapping']['default_fields'] as $element => $properties) {
    if (isset($properties['#type']) && $properties['#type'] == 'select') {
      $form['diffbot_mapping']['default_fields'][$element]['#options'] =
        diffbot_ui_assigned_field_get($selected);
    }
  }
  return $form;
}

function diffbot_ui_form_set_particular_attributes($form) {
  foreach ($form['diffbot_mapping']['default_fields'] as $element => $attibutes) {
    if ($element == 'url') {
      $form['diffbot_mapping']['default_fields'][$element]['#required'] = TRUE;
    }
  }
  return $form;
}

/**
 * Set the default values to the Mapping form.
 * @param  array $form    Form to set values.
 * @param  Mapping $mapping Object to get the default values.
 * @return array  $form      Form modified.
 */
function diffbot_ui_form_set_default_values($form, $mapping) {
  // Setting the dafault values to the default fields.
  foreach ($mapping->fields['default_fields'] as $field => $value) {
    if ( $mapping->get_value($field) != '') {
      $form['diffbot_mapping']['default_fields'][$field]['#default_value'] =
        $mapping->get_value($field);
    }
  }
  $form['diffbot_mapping']['default_fields']['html_filter']['#default_value'] =
    $mapping->fields['default_fields']['html']['html_filter'];

  return $form;
}

function diffbot_ui_form_set_weight($form, $mapping) {
  $default_fields = $mapping->fields['default_fields'];
  $i = 0;
  $last_field = '';
  foreach ($default_fields as $field => $values) {
    $form['diffbot_mapping']['default_fields'][$field]['#weight'] = $i;
    $i = ($field == 'html') ? $i+1 : $i ;
    $i++;
    $last_field = $field;
  }

  // Closing the diffbot-mapping div on the last element of the fieldset form.
  $form['diffbot_mapping']['default_fields'][$last_field]['#suffix'] = '</div>';
  return $form;
}

function diffbot_ui_form_set_custom_elements($form) {
  $form['diffbot_mapping']['default_fields']['html_filter'] = array(
    '#type' => 'select',
    '#title' => 'HTML Filter',
    '#description' => t('Text formats define the HTML tags, code, and other
      formatting that can be used when save text.'),
    '#options' => diffbot_ui_get_text_formatter(),
    '#weight' => $form['diffbot_mapping']['default_fields']['html']['#weight']+1,
  );
  return $form;
}

function diffbot_ui_get_field_default_fieldset($form, $form_state) {
  return $form['diffbot_mapping']['default_fields'];
}

function diffbot_ui_get_content_types() {
  $content_types_list = node_type_get_types();
  foreach ($content_types_list as $key => $type) {
    $content_types[$key] = $type->name;
  }
  $content_types = diffbot_ui_remove_content_type_used($content_types);
  return $content_types;
}

function diffbot_ui_assigned_field_get($key = '') {
  $list_of_fields = field_info_instances("node", $key);
  $fields[''] = '- None -';
  $fields['node_title'] = 'Node Title';
  foreach ($list_of_fields as $field_name => $field ) {
    $fields[$field_name] = $field['label'];
  }
  if (isset($fields)) {
    return $fields;
  }
  else {
    return array();
  }
}

function diffbot_ui_field_type_get() {
  $field_types = array(
    'text' => 'Text',
    'terms' => 'Terms',
  );
  return $field_types;
}


function diffbot_ui_remove_content_type_used($content_types) {
  $query = db_select('diffbot_mapping', 'n')
    ->fields('n', array('content_type'))
    ->execute()
    ->fetchAll();
  foreach ($query as $name) {
    unset( $content_types[$name->content_type] );
  }
  return $content_types;
}


function diffbot_ui_mapping_form_valid_values($form_state) {
  $destination_url_field_info = field_info_field($form_state['values']['url']);
  if (strcmp($destination_url_field_info['type'], 'text') != 0) {
    form_set_error('url',
      t('You must enter a Field Type <strong>Text</strong> to Destination URL'));
  }
}

function diffbot_ui_get_text_formatter() {
  $user = $GLOBALS['user'];
  $formatters = filter_formats($user);
  foreach ($formatters as $format => $settings) {
    $formatters_options[$format] = $settings->name;
  }
  return $formatters_options;
}
