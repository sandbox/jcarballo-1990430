<?php

/**
 * @file Rules integration for the Diffbot module
 */


/**
 * Implements hook_rules_event_info().
 */
function diffbot_rules_event_info() {
  $items = array(
    'diffbot_rules_event_before_save' => array(
      'label' => t('Before saving an node whose content has been extracted with Diffbot.'),
      'group' => 'Diffbot',
      'variables' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node where the content will be saved.'),
          'skip save' => TRUE,
        ),
      ),
    ),
    'diffbot_rules_event_after_save' => array(
      'label' => t('After saving an node whose content has been extracted with Diffbot.'),
      'group' => 'Diffbot',
      'variables' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node where the content was saved.'),
          'skip save' => TRUE,
        ),
      ),
    ),
  );

  return $items;
}

/**
 * Implements hook_rules_condition_info().
 */
function diffbot_rules_condition_info() {
  $condition = array(
    'diffbot_rules_condiciont_extraction' => array(
      'label' => t('Content was extracted'),
      'group' => t('Diffbot'),
      'arguments' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node where the extracted content was saved'),
        )
      ),
    ),
  );

  return $condition;
}

/**
 * Implements hook_rules_action_info().
 */
function diffbot_rules_action_info() {
  $actions = array(
    'diffbot_rules_action_extraction' => array(
      'label' => t('Extract content'),
      'group' => t('Diffbot'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node where save the extracted content'),
          'save' => TRUE,
        ),
      ),
    ),
    'diffbot_rules_action_skip_extraction' => array(
      'label' => t('Skip text extraction'),
      'group' => t('Diffbot'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node to be marked as skipped'),
        ),
      ),
    ),
  );

  return $actions;
}

function diffbot_rules_action_extraction($node) {
  diffbot_content_extraction($node);
}

function diffbot_rules_action_skip_extraction($node) {
  diffbot_content_extraction($node->diffbot_skip=TRUE);
}

function diffbot_rules_condiciont_extraction($node) {
  return $node->diffbot_extraction;
}
