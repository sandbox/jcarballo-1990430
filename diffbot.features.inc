<?php

/**
 * @file Feature integration.
 */

/**
 * Implementation of hook_features_export_options. [component_hook]
 *
 * @return array
 *   A keyed array of items, suitable for use with a FormAPI select or
 *   checkboxes element.
 */
function diffbot_features_export_options() {
  $content_types_list = node_type_get_types();
  $options = array();
  $query = " SELECT id, content_type FROM {diffbot_mapping} ";
  $params = array();
  $result = db_query($query, $params);

  foreach ($result as $record) {
    $options[$record->content_type] = $content_types_list[$record->content_type]->name;
  }
  return $options;
}

/**
* Implementation of hook_features_export [component hook]
*
* @param array $data
* this is the machine name for the component in question
* @param array &$export
* array of all components to be exported
* @param string $module_name
* The name of the feature module to be generated.
* @return array
* The pipe array of further processors that should be called
*/
function diffbot_features_export($data, &$export, $module_name) {
  $export['dependencies']['diffbot'] = 'diffbot';

  foreach ($data as $component) {
    $export['features']['diffbot'][$component] = $component;
  }
  return array();
}

/**
 * Implements hook_features_export_render()
 */
function diffbot_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $mappings = array();';
  $code[] = '';
  foreach ($data as $node_type) {
    //retrieve the contest information
    $item = diffbot_load_mapping($node_type);
    unset($item->id);
    //add the contest code to the feature
    if (isset($item->contentType)) {
      $code[] = '  $mappings[] = ' . features_var_export($item, '  ') . ';';
    }
  }
  $code[] = '  return $mappings;';
  $code = implode("\n", $code);
  return array('diffbot_features_default_settings' => $code);
}

/**
* Implements hook_features_rebuild().
*/
function diffbot_features_rebuild($module) {
  $items = module_invoke($module, 'diffbot_features_default_settings');
  foreach ($items as $mapping) {
    _diffbot_config_set_data($mapping);
  }
}

/**
 * Implements hook_features_revert().
 */
function diffbot_features_revert($module) {
  dpm($module);
  diffbot_features_rebuild($module);
}
